unit StringCollector;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  TStringCollector = class
    public
      constructor Create();
      procedure take(var s : String);
      procedure newLine(number : Integer = 1);
      procedure addDashLine;
      procedure add(s : String);
      procedure apply;
      procedure stash;

    private
      buffer : string;
      slaveString : ^String;
  end;

implementation

//-----------------------------------------------------------------------------
// CONSTRUCTOR
//-----------------------------------------------------------------------------
constructor TStringCollector.Create();
begin
  buffer := '';
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.take(var s : String);
begin
  slaveString := @s;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.newLine(number : Integer);
var
  i : Integer;
begin
  for i := 1 to number do
    begin
      buffer := buffer + #13;
    end;
end;


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.addDashLine;
begin
  buffer := buffer + '//-----------------------------------------------------';
  buffer := buffer + '------------------------' + #13;
end;


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.add(s : String);
begin
  buffer := buffer + s + #13;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.apply;
begin
  slaveString^ := buffer;
  buffer := '';
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TStringCollector.stash;
begin
  buffer := slaveString^;
end;

end.

