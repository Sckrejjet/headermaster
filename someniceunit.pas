unit SomeNiceUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, shlobj;

  function GetSpecPath(CSIDL: word) : String;
  function PATH_ProgramFiles : String;



implementation

function GetSpecPath(CSIDL: word) : String;
var
  s : String;
begin
  SetLength(s, MAX_PATH);
  if not SHGetSpecialFolderPath(0, PChar(s), CSIDL, true) then
    s := '';
  Result := PChar(s);
end;


function PATH_ProgramFiles : String;
begin
  Result := GetSpecPath(CSIDL_PERSONAL);
end;

end.

