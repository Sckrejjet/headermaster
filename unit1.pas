unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Clipbrd, ExtCtrls, Buttons, windows, StringCollector, SomeNiceUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtnClassHeader: TBitBtn;
    buttonCreateClass: TButton;
    ButtonClassHeader: TButton;
    ButtonMethodHeader: TButton;
    ButtonSlotHeader: TButton;
    ButtonSplitter: TButton;
    checkBoxDoxygen: TCheckBox;
    comboBoxUsers: TComboBox;
    comboBoxBehavior: TComboBox;
    editName: TEdit;
    imageAddUser: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Timer1: TTimer;
    procedure ButtonClassHeaderClick(Sender: TObject);
    procedure buttonCreateClassClick(Sender: TObject);
    procedure ButtonMethodHeaderClick(Sender: TObject);
    procedure ButtonSlotHeaderClick(Sender: TObject);
    procedure ButtonSplitterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure imageAddUserClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure clickDelete(Sender: TObject);
    function  dateToStr : String;
    procedure behavior();
  private
    { private declarations }
    active_   : boolean;
    collector : TStringCollector;
    listUsers  : TStringList;
    {privare methods}
    function makeSplitter(someStr : String) : String;
    procedure loadUsers();
    procedure saveUser(user : UTF8String);
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation


{$R *.lfm}

{ TForm1 }

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.ButtonClassHeaderClick(Sender: TObject);
var
  resultString : String = '';
begin
  with collector do
  begin
    take(resultString);
    addDashLine;
    add('//');
    add('//      ' + editName.Text);
    add('//      (c) РГУПС, ВЖД ' + dateToStr);
    add('//      Разработал: ' + comboBoxUsers.Text);
    add('//');
    addDashLine;

    if checkBoxDoxygen.Checked then
      begin
        add('/*!');
        add(' *  \file');
        add(' *  \brief ' + editName.Text);
        add(' *  \copyright РУГПС, ВЖД');
        add(' *  \author ' + comboBoxUsers.Text);
        add(' *  \date ' + dateToStr);
        add(' */');
      end;
    apply;
  end;

  Clipboard.SetTextBuf(PChar(resultString));

  behavior();
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.buttonCreateClassClick(Sender: TObject);
var
  resultString : String;
begin
  with collector do
    begin
      take(resultString);
      add('class ' + editName.Text);
      add('{');
      add('public:');
      add('    ' + editName.Text + '();');
      add('    ~' + editName.Text + '();');
      newLine(2);
//      add('');
//      add('');
      add('private:');
//      add('');
      newLine();
      add('};');
      apply();
    end;

  Clipboard.SetTextBuf(PChar(resultString));

  behavior();
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.ButtonMethodHeaderClick(Sender: TObject);
var
  resultString : String = '';
begin
  with collector do
  begin
    take(resultString);
    addDashLine;
    add('// ' + editName.Text);
    addDashLine;
    apply;
  end;

  Clipboard.SetTextBuf(PChar(resultString));

  behavior();
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.ButtonSlotHeaderClick(Sender: TObject);
var
  resultString : String = '';
begin
  with collector do
  begin
    take(resultString);
    addDashLine;
    add('// (слот) ' + editName.Text);
    addDashLine;
    apply;
  end;

  Clipboard.SetTextBuf(PChar(resultString));

  behavior();
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.ButtonSplitterClick(Sender: TObject);
begin
  Clipboard.SetTextBuf(PChar(makeSplitter(editName.Text)));

  behavior();
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  Self.FormStyle := fsSystemStayOnTop;
  active_ := True;
  collector := TStringCollector.Create();
  listUsers := TStringList.Create();
  loadUsers;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.FormDestroy(Sender: TObject);
begin
  collector.Free;
  listUsers.Free;
end;

procedure TForm1.imageAddUserClick(Sender: TObject);
var
  s : UTF8String;
begin
  s := InputBox('Добавление нового автора',
                'Введите фамилию и инициалы нового автора.',
                'Иванов И. И.');
  comboBoxUsers.Items.Append(s);
  saveUser(s);
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.Timer1Timer(Sender: TObject);
var
  i : integer = 0;
  change : Boolean;
begin
  change := active_;

  if Form1.Handle = GetForegroundWindow()then
    begin
      if active_ then
        begin
          active_:=false;
          Self.AlphaBlendValue := 255;
        end;
    end
  else
    begin
      active_:=true;
      Self.AlphaBlendValue := 150;
    end;

  if change <> active_ then
    begin
      for i := 0 to Self.ComponentCount - 1 do
        begin
          if Self.Components[i] is TControl then
            TControl(Self.Components[i]).Enabled:= not active_;
        end;
    end;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.clickDelete(Sender: TObject);
begin
  editName.Clear;
  editName.SetFocus;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function TForm1.dateToStr : String;
var
  strDate : String;
  bufDate : TDate;
  i : Integer;
begin
  bufDate := Now;
  strDate := FormatDateTime('dd/mm/yyyy', bufDate);

  for i := 1 to Length(strDate) do
  begin
    if strDate[i] = '.' then
      begin
        Delete(strDate, i, 1);
        Insert('/', strDate, i);
      end;
  end;
  Result := strDate;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.behavior;
begin
  if comboBoxBehavior.ItemIndex = 1 then
    begin
      Application.Minimize;
    end
  else if comboBoxBehavior.ItemIndex = 2 then
    begin
      Self.Close;
    end;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
function TForm1.makeSplitter(someStr: string): string;
var
  i : Integer;
  bufStr : string;
begin
  bufStr := '//';
  for i := 1 to (76 - Length(someStr))div(2) do
    begin
      bufStr := bufStr + '-';
    end;
  bufStr := bufStr + ' ';
  bufStr := bufStr + someStr + ' ';
  while Length(bufStr) < 79 do
    begin
      bufStr := bufStr + '-';
    end;
  Result := bufStr;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.loadUsers();
var
  F : TextFile;
  usersPath : UTF8String;
  usersFile : UTF8String;
begin
  usersPath := PATH_ProgramFiles + '\HeaderMaster\';
  usersFile := usersPath + 'users.txt';

  if not FileExists(usersFile) then
    begin
      if not DirectoryExists(usersPath) then
        CreateDir(usersPath);
      AssignFile(F, usersFile);
      Rewrite(F);
      WriteLn(F, 'Ковшиков С. В.');
      CloseFile(F);
    end;

  if FileExists(usersFile) then
    begin
      listUsers.LoadFromFile(usersFile);
      comboBoxUsers.Items := listUsers;
      if comboBoxUsers.Items.Count > 0 then
        comboBoxUsers.ItemIndex := 0;
    end;
end;



//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
procedure TForm1.saveUser(user : UTF8String);
var
  F : TextFile;
  usersPath : UTF8String;
  usersFile : UTF8String;
begin
  usersPath := PATH_ProgramFiles + '\HeaderMaster\';
  usersFile := usersPath + 'users.txt';

  if FileExists(usersFile) then
    begin
      AssignFile(F, usersFile);
      Append(F);
      WriteLn(F, user);
      CloseFile(F);
    end;
end;

end.

